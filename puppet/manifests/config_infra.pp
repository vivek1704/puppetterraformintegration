# Ensure required packages are installed
package { ['nginx', 'mysql-server', 'php']:
  ensure => installed,
}

# Configure Nginx
file { '/etc/nginx/nginx.conf':
  ensure  => file,
  source  => 'puppet:///modules/nginx/nginx.conf',
  require => Package['nginx'],
  notify  => Service['nginx'],
}

# Configure MySQL
file { '/etc/mysql/mysql.conf.d/mysqld.cnf':
  ensure  => file,
  source  => 'puppet:///modules/mysql/mysqld.cnf',
  require => Package['mysql-server'],
  notify  => Service['mysql'],
}

# Configure PHP
file { '/etc/php/7.4/fpm/php.ini':
  ensure  => file,
  source  => 'puppet:///modules/php/php.ini',
  require => Package['php'],
  notify  => Service['php7.4-fpm'],
}

# Start and enable services
service { 'nginx':
  ensure  => running,
  enable  => true,
}

service { 'mysql':
  ensure  => running,
  enable  => true,
}

service { 'php7.4-fpm':
  ensure  => running,
  enable  => true,
}

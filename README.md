## PuppetTerraformIntegration
. The objective of this repository is to automate the configuration management of infrastructure resources provisioned using Terraform using Puppet.


## Add your files

```
git init
git add .
git commit -m "initial"
git remote add origin https://gitlab.com/vivek1704/puppetterraformintegration.git
git branch -M main
git push -uf origin main
```

## Steps for creating an ec2 using Terraform and automating the configuration management
- create two folders i.e Terraform and Puppet into you local system.
- inside Terraform folder, create a main.tf file and provide all the configuration related to ec2 instance and provisioning of puppet configuration.
- Then go to into Puppet folder, create an another folder i.e manifests and inside this create a file "config_infra.pp"

## Steps for running Terraform
- terraform init, to initialize the terraform
- terraform validate, to check the syntax and other validate related to the file
- terraform plan
- terraform apply

After applying all the steps you will able to create an ec2 instance with configuration management of puppet.

## Troubleshooting
If you get any error then ensure the configuration of main.tf file and also verify that you have a valid private key for aws instance and then again run Terraform steps. 
